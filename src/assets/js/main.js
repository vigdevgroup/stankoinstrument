import 'jquery-mask-plugin'
import 'ion-rangeslider'
import initSliders from "./parts/sliders.js";
import initForm from './parts/form.js'
import initModal from './parts/modal.js'
import initMobileMenu from "./parts/mobile-menu.js";
import initScrollAnimate from './parts/scroll-animate.js'
import initCatalogMenu from './parts/catalog-menu.js'
import initAnchorLinks from "./parts/anchorLinks.js";
import initProductDescription from "./parts/productDescription.js";
import initRangeSlider from "./parts/rangeSlider.js";
import initFilters from './parts/filters.js'

$(function() {
    initSliders()
    initForm()
    initModal()
    initMobileMenu()
    initScrollAnimate()
    initCatalogMenu()
    initAnchorLinks()
    initProductDescription()
    initRangeSlider()
    initFilters()
})



