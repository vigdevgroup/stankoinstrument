export default function initProductDescription() {
    $('.product-description__head').click(function() {
        if (window.innerWidth > 500) return

        const body = $(this).closest('.product-description__row').find('.product-description__body')

        if ($(this).hasClass('product-description__head_opened')) {
            $(this).removeClass('product-description__head_opened')
            $(body).slideUp(300)
        } else {
            $(this).addClass('product-description__head_opened')
            $(body).slideDown(300)
        }
    })
}