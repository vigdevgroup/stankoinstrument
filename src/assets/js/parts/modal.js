import {closeMobileMenu} from "./mobile-menu.js";

export default function initModal() {
    $('.btn-modal').on('click', function () {
        const module = this.dataset.module

        if ($('.header').hasClass('header_menu-opened')) {
            closeMobileMenu()
        }

        openModal(module)
    })
}

export function openModal(module) {
    const modal = $(".modal").get(0);
    const moduleEl = $(`.${module}-module`).get(0)
    const body = $(modal).find('.modal__body').get(0)
    body.innerHTML = moduleEl.innerHTML

    $(modal).fadeIn(300)

    initInputs($(modal))

    setTimeout(() => {
        $('body').addClass('_no-scroll')
        document.addEventListener("click", closeModal);
        document.addEventListener("keyup", closeModal);
    }, 0);
}

export function changeModal(module) {
    const modal = $(".modal").get(0);
    const body = $(modal).find('.modal__body').get(0)
    const moduleEl = $(`.${module}-module`).get(0)

    $(body).addClass('modal__body_hide')
    setTimeout(() => {
        destroyInputs($(modal))
        body.innerHTML = moduleEl.innerHTML
        $(body).removeClass('modal__body_hide')
    }, 210)
}

export function closeModal(event) {
    let condition = false
    if (event.code && event.code === 'Escape') condition = true
    if (event.target && event.target.classList.contains('modal__inner')) condition = true
    if (event.target && event.target.closest('.modal__close')) condition = true

    if (condition) {
        const modal = $(".modal").get(0);

        $(modal).fadeOut(300)
        document.removeEventListener("click", closeModal);
        document.removeEventListener("keyup", closeModal);
        $('body').removeClass('_no-scroll')

        setTimeout(() => {
            destroyInputs($(modal))
        }, 310)
    }
}

function initInputs(parent) {
    parent.find('input[type="tel"]').not('.module input').each(function () {
        $(this).mask("+7 (000) 000-00-00");
    });
}

function destroyInputs(parent) {
    parent.find('input[type="tel"]').each(function () {
        $(this).unmask();
    });
}
