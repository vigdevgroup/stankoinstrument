import Swiper from 'swiper/bundle'

export default function initSliders() {
    $('.product-slider').each(function() {
        new Swiper($(this).find('.product-slider__inner').get(0), {
            slidesPerView: 'auto',
            spaceBetween: 0,
            freeMode: true,
            grabCursor: true,
            navigation: {
                prevEl: $(this).find('.product-slider__arrow_prev').get(0),
                nextEl: $(this).find('.product-slider__arrow_next').get(0)
            },
        })
    })

    new Swiper('.reviews-slider__inner', {
        slidesPerView: 1,
        spaceBetween: 20,
        pagination: {
          el: '.reviews-slider__dots'
        },
        navigation: {
            prevEl: '.reviews-slider__arrow_prev',
            nextEl: '.reviews-slider__arrow_next'
        },
        breakpoints: {
            501: {
                slidesPerView: 'auto',
                spaceBetween: 0,
                freeMode: true,
                grabCursor: true,
            }
        }
    })

    new Swiper('.partners-slider__inner', {
        slidesPerView: 2,
        slidesPerGroup: 2,
        spaceBetween: 30,
        pagination: {
            el: '.partners-slider__dots'
        },
        navigation: {
            prevEl: '.partners-slider__arrow_prev',
            nextEl: '.partners-slider__arrow_next'
        },
        breakpoints: {
            501: {
                slidesPerView: 'auto',
                spaceBetween: 0,
                freeMode: true,
                grabCursor: true,
            }
        }
    })

    const productInfoThumbs = new Swiper('.product-info__thumbs-inner', {
        slidesPerView: 'auto',
        freeMode: true,
        spaceBetween: 10,
        navigation: {
            prevEl: '.product-info__thumbs-arrow.product-info__thumbs-arrow_prev',
            nextEl: '.product-info__thumbs-arrow.product-info__thumbs-arrow_next',
        },
        breakpoints: {
            1281: {
                direction: 'vertical',
            }
        }
    })

    if (productInfoThumbs) {
        new Swiper('.product-info__preview', {
            slidesPerView: 'auto',
            freeMode: true,
            spaceBetween: 20,
            thumbs: {
                swiper: productInfoThumbs
            },
            breakpoints: {
                1041: {
                    freeMode: false,
                    slidesPerView: 1,
                    slidesPerGroup: 1,
                    spaceBetween: 0,
                    effect: 'fade',
                    fadeEffect: {
                        crossFade: true
                    },
                }
            }
        })
    }
}