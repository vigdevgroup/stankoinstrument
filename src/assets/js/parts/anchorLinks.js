export default function initAnchorLinks() {
    $('.anchor-link').on('click', function (event) {
        event.preventDefault()

        const target = $(this).data('target')
        if (!target) return

        scrollToElem(target)
    })

    function scrollToElem(target) {

        const elem = document.querySelector('.' + target)
        if (!elem) return

        const top = $(elem).offset().top

        let time = Math.abs(top - $(window).scrollTop() + $(window).height()) / 2.5
        if (time < 1000) time = 1000

        let scrollTop = top - 50 - $('.header').height()

        $('html, body').animate({
            scrollTop
        }, time);
    }
}
