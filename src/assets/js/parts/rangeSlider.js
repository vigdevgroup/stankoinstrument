export default function initRangeSlider() {
    $('.range-slider').each(function() {
        const fromField = $(this).find('.range-slider__input_from').get(0)
        const toField = $(this).find('.range-slider__input_to').get(0)
        const rangeField = $(this).find('.range-slider__field').get(0)

        $(rangeField).ionRangeSlider({
            type: 'double',
            skin: 'round',
            hide_min_max: true,
            hide_from_to: true,
            extra_classes: 'range-slider__field',
            onStart: (data) => { updageRangeInputs(data, fromField, toField) },
            onChange: (data) => { updageRangeInputs(data, fromField, toField) },
            onFinish: (data) => { updageRangeInputs(data, fromField, toField) }
        })

        const instance = $(rangeField).data('ionRangeSlider')

        const min = instance.options.min
        const max = instance.options.max

        $(fromField).on('input', function() {
            $(fromField).val($(fromField).val().replace(/\D/, ''))

            let val = $(fromField).prop("value");

            if (val < min) val = min
            else if (val > instance.old_to) val = instance.old_to

            instance.update({
                from: val
            });
        })
        $(fromField).on('blur', function() {
            let val = $(fromField).prop("value");

            if (val < min) val = min
            else if (val > instance.old_to) val = instance.old_to

            $(fromField).prop("value", val);
        })

        $(toField).on('input', function() {
            $(toField).val($(toField).val().replace(/\D/, ''))

            let val = $(toField).prop("value");

            if (val < instance.old_from) val = instance.old_from
            else if (val > max) val = max

            instance.update({
                to: val
            });
        })
        $(toField).on('blur', function() {
            let val = $(toField).prop("value");

            if (val < instance.old_from) val = instance.old_from
            else if (val > max) val = max

            $(toField).prop("value", val);
        })
    })
}

function updageRangeInputs(data, fromField, toField) {
    const from = data.from;
    const to = data.to;

    $(fromField).prop("value", from);
    $(toField).prop("value", to);
}