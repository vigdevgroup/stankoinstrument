export default function initMobileMenu() {
    $('.header__mobile-btn').click(function() {
        if ($('.header').hasClass('header_menu-opened')) {
            closeMobileMenu()
        } else {
            openMobileMenu()
        }
    })

    window.addEventListener('resize', function() {
        if (window.innerWidth > 1460 && $('.header').hasClass('header_menu-opened')) {
            closeMobileMenu()
        }
    })

    $('.header__mobile-head_drop').click(function() {
        const body = $(this).closest('.header__mobile-item').find('.header__mobile-body')

        if ($(this).hasClass('header__mobile-head_opened')) {
            $(body).slideUp(300)
            $(this).removeClass('header__mobile-head_opened')
        } else {
            $(body).slideDown(300)
            $(this).addClass('header__mobile-head_opened')
        }
    })
}

function openMobileMenu() {
    $('.header').addClass('header_menu-opened')
    $('.header__mobile').fadeIn(300)
    document.addEventListener('keyup', closeMobileMenu)
    $('body').addClass('_no-scroll')
}

export function closeMobileMenu(e) {
    if (!e || e.code === 'Escape') {
        $('.header').removeClass('header_menu-opened')
        $('.header__mobile').fadeOut(300)
        document.removeEventListener('keyup', closeMobileMenu)
        $('body').removeClass('_no-scroll')
    }
}