export default function initCatalogMenu() {
    $('.header__catalog-btn').click(function() {
        if($(this).hasClass('header__catalog-btn_active')) {
            closeCatalog()
        } else {
            openCatalog()
        }
    })

    window.addEventListener('resize', function() {
        if (window.innerWidth <= 1460 && $('.header__catalog-btn').hasClass('header__catalog-btn_active')) {
            closeCatalog()
        }
    })

    function openCatalog() {
        $('.header__catalog').fadeIn(300)
        $('.header__catalog-btn').addClass('header__catalog-btn_active')
        $('.header__inner').addClass('header__inner_catalog-opened')
        document.addEventListener('keyup', closeCatalog)
        document.addEventListener('click', closeCatalog)
    }

    function closeCatalog(e) {
        if (!e || e.code === 'Escape' || (!e.target.closest('.header__catalog') && !e.target.closest('.header__catalog-btn'))) {
            $('.header__catalog').fadeOut(300)
            $('.header__catalog-btn').removeClass('header__catalog-btn_active')
            $('.header__inner').removeClass('header__inner_catalog-opened')
            document.removeEventListener('keyup', closeCatalog)
            document.removeEventListener('click', closeCatalog)
        }
    }


    let timeout = null

    $('.header__catalog-tab').hover(function() {

        if (!$(this).hasClass('header__catalog-tab_active')) {
            const list = $('.header__catalog-list').get(0)
            const module = $(`.module.${this.dataset.target}-catalog-module`).get(0)

            $('.header__catalog-tab_active').removeClass('header__catalog-tab_active')
            $(this).addClass('header__catalog-tab_active')

            $(list).addClass('header__catalog-list_hide')

            if (timeout) {
                clearTimeout(timeout)
                timeout = null
            }

            timeout = setTimeout(() => {
                list.innerHTML = module.innerHTML
                $(list).removeClass('header__catalog-list_hide')
                timeout = null
            }, 160)
        }

    })
}