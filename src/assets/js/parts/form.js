import {openModal, changeModal} from "./modal.js";

export default function initForm() {
    initInputs($('body'))

    $('body').on("change", function (event) {
        if (event.target.dataset.checking === 'false') return
        checkInputHandler(event.target);
    });

    document.addEventListener("submit", checkForm);
}

function initInputs(parent) {
    parent.find('input[type="tel"]').each(function () {
        $(this).mask("+7 (000) 000-00-00");
    });
}

function checkInputHandler(target) {
    const form = target.closest('form')
    if (form && form.dataset.checking === 'false') return

    checkInput(target);
}

function checkInputValid(target) {
    let type = target.type;
    if (target.name === 'inn') type = target.name
    if (target instanceof NodeList) {
        type = target[0].type;
    }
    let valid = false;
    let regexp

    switch (type) {
        case "text":
            if (target.dataset.onlywords)
                regexp = /^[a-zA-Zа-яА-Я][a-zA-Zа-яА-Я\s]+$/;
            else regexp = /^.+$/;
            if (regexp.test(target.value.trim()) || target.value === '') valid = true;
            break;
        case "email":
            regexp = /^[\w-.]+@([-\w]+\.)+[-\w]+$/;
            if (regexp.test(target.value.trim()) || target.value === '') valid = true;
            break;
        case "textarea":
            if (target.value.trim().length > 5 || target.value === '') valid = true;
            break;
        case "tel":
            if (target.value.trim().length === 18 || target.value === '') valid = true;
            break;
        case "radio":
            if (target.value) valid = true;
            break;
        case "checkbox":
            if (target.checked) valid = true;
            break;
        case "select-one":
            if (target.value) valid = true;
            break;
        case "inn":
            if (target.value.length === 13 || target.value.length === 15) valid = true;
            break;
    }

    return valid;
}

function checkInputEmpty(target) {
    let type = target.type;
    if (target instanceof NodeList) {
        type = target[0].type;
    }
    let empty = false;

    switch (type) {
        default:
            if (!target.value) empty = true;

            break;
    }

    return empty;
}

function checkInput(target) {
    let field = target;
    if (target instanceof NodeList) {
        field = target[0];
    }

    if (checkInputValid(target)) {
        field.classList.remove("input_error");
        return true;
    } else if (checkInputEmpty(target)) {
        field.classList.remove("input_error");
        return false;
    } else {
        field.classList.add("input_err");
        return false;
    }
}

function checkForm(event) {
    event.preventDefault();

    let form = event.target;
    if (form.dataset.checking === 'false') return;

    let errs = false;

    let data = new FormData();
    let formEls = $(form).serializeArray();
    $(form)
        .find('input[type="radio"], input[type="checkbox"]')
        .each(function () {
            const name = this.name;
            if (!formEls.find((el) => el.name === name)) {
                formEls.push({name, value: ""});
            }
        });

    $.each(formEls, function () {
        let target = form[this.name];
        let field = target;
        if (target instanceof NodeList) {
            field = target[0];
        }

        if ((checkInputEmpty(target) && target.dataset.required !== 'false') || !checkInputValid(target)) {
            errs = true;
            field.classList.add("input_error");
        } else {
            field.classList.remove("input_error");
        }
    });

    if (errs) return;

    $.each(formEls, function () {
        data.append(this.name, this.value);
    });

    const btn = $(form).find('button[type="submit"]').get(0);

    btn.disabled = true;

    // Отправка...


    btn.disabled = false;
    clearForm(form, formEls);

    if (form.closest('.modal')) {
        changeModal('modal-thanks')
    } else {
        openModal('modal-thanks')
    }
}

function clearForm(form, formEls) {
    $.each(formEls, function () {
        const target = form[this.name];
        let type = target.type;
        if (target instanceof NodeList) {
            type = target[0].type;
        }

        if (["text", "email", "textarea", "tel", "file"].includes(type)) {
            target.value = "";
        }
    });
}
