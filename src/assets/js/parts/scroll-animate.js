export default function initScrollAnimate() {
    const options = {
        threshold: [0.4],
    };

    const observer = new IntersectionObserver(onEntry, options);
    const elements = document.querySelectorAll(".scroll-animate");

    for (const elm of elements) {
        observer.observe(elm);
    }
}

function onEntry(entry) {
    for (const change of entry) {
        if (change.isIntersecting) {

            if (change.target.classList.contains('js-counter') && !change.target.classList.contains('scroll-animate_animated')) {
                const targets = change.target.querySelectorAll('span[data-value]')

                for (const target of targets) {
                    countNumber(target, parseFloat(target.dataset.value))
                }
            }

            change.target.classList.add("scroll-animate_animated");
        }
    }
};

function countNumber(field, max) {
    let val = 0
    field.innerHTML = val
    let step = 0

    if (Number.isInteger(max)) step = Math.round(max / 15)
    else step = 0.05

    let interval = setInterval(() => {
        if (val < max) {
            val += step
            if (Number.isInteger(max)) field.innerHTML = val
            else field.innerHTML = val.toFixed(2)
        } else {
            field.innerHTML = max
            clearInterval(interval)
        }
    }, 40)
}
