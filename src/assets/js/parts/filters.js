import {closeMobileMenu} from "./mobile-menu.js";

export default function initFilters() {
    const maxDeviceSize = 992

    $('.catalog__filters-title_drop').click(function() {
        if (window.innerWidth > maxDeviceSize) return

        if ($(this).hasClass('catalog__filters-title_opened')) {
            $(this).removeClass('catalog__filters-title_opened')
            $(this).closest('.catalog__filters-row').find('.catalog__filters-body').slideUp(300)
        } else {
            $(this).addClass('catalog__filters-title_opened')
            $(this).closest('.catalog__filters-row').find('.catalog__filters-body').slideDown(300)
        }
    })

    $('.catalog__btns-item_filter').click(openFilters)
    $('.catalog__filters-close').click(closeFilters)
    $('.catalog__filters-control_apply').click(closeFilters)
}

function openFilters() {
    $('.catalog__filters').fadeIn(300)
    document.addEventListener('keyup', closeFilters)
    $('body').addClass('_no-scroll')
}

 function closeFilters(event) {
     if (event.target || event.code === 'Escape') {
         $('.catalog__filters').fadeOut(300)
         document.removeEventListener('keyup', closeFilters)
         $('body').removeClass('_no-scroll')
     }
 }
